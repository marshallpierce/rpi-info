use super::*;

use std::path;

#[test]
fn old_b_10() {
    let rev_code = 0x2;
    let expected = Revision {
        revision_code: rev_code,
        memory: MemorySize::MiB256,
        mfg: Manufacturer::Egoman,
        model: Model::B,
        revision_num: RevisionNum::V1(1, 0),
        processor: Processor::BCM2835,
    };
    assert_eq!(expected, parse_num(rev_code).unwrap());
}

#[test]
fn old_a_20() {
    let rev_code = 0x8;
    let expected = Revision {
        revision_code: rev_code,
        memory: MemorySize::MiB256,
        mfg: Manufacturer::SonyUK,
        model: Model::A,
        revision_num: RevisionNum::V1(2, 0),
        processor: Processor::BCM2835,
    };
    assert_eq!(expected, parse_num(rev_code).unwrap());
}

#[test]
fn old_cm1_10() {
    let rev_code = 0x14;
    let expected = Revision {
        revision_code: rev_code,
        memory: MemorySize::MiB512,
        mfg: Manufacturer::Embest,
        model: Model::CM1,
        revision_num: RevisionNum::V1(1, 0),
        processor: Processor::BCM2835,
    };
    assert_eq!(expected, parse_num(rev_code).unwrap());
}

#[test]
fn new_cm3_10() {
    let rev_code = 0xa020a0;
    let expected = Revision {
        revision_code: rev_code,
        memory: MemorySize::MiB1024,
        mfg: Manufacturer::SonyUK,
        model: Model::CM3,
        revision_num: RevisionNum::V2(0),
        processor: Processor::BCM2837,
    };
    assert_eq!(expected, parse_num(rev_code).unwrap());
}

#[test]
fn new_3b_12() {
    let rev_code = 0xa32082;
    let expected = Revision {
        revision_code: rev_code,
        memory: MemorySize::MiB1024,
        mfg: Manufacturer::SonyJapan,
        model: Model::B3,
        revision_num: RevisionNum::V2(2),
        processor: Processor::BCM2837,
    };
    assert_eq!(expected, parse_num(rev_code).unwrap());
}

#[test]
fn new_3bplus_13() {
    let rev_code = 0xa020d3;
    let expected = Revision {
        revision_code: rev_code,
        memory: MemorySize::MiB1024,
        mfg: Manufacturer::SonyUK,
        model: Model::B3Plus,
        revision_num: RevisionNum::V2(3),
        processor: Processor::BCM2837,
    };
    assert_eq!(expected, parse_num(rev_code).unwrap());
}

#[test]
fn new_3b_stadium_12() {
    let rev_code = 0xa52082;
    let expected = Revision {
        revision_code: rev_code,
        memory: MemorySize::MiB1024,
        mfg: Manufacturer::Stadium,
        model: Model::B3,
        revision_num: RevisionNum::V2(2),
        processor: Processor::BCM2837,
    };
    assert_eq!(expected, parse_num(rev_code).unwrap());
}

#[test]
fn new_3b_embest_13() {
    let rev_code = 0xa22083;
    let expected = Revision {
        revision_code: rev_code,
        memory: MemorySize::MiB1024,
        mfg: Manufacturer::Embest,
        model: Model::B3,
        revision_num: RevisionNum::V2(3),
        processor: Processor::BCM2837,
    };
    assert_eq!(expected, parse_num(rev_code).unwrap());
}

#[test]
fn new_cm3plus_10() {
    let rev_code = 0xa02100;
    let expected = Revision {
        revision_code: rev_code,
        memory: MemorySize::MiB1024,
        mfg: Manufacturer::SonyUK,
        model: Model::CM3Plus,
        revision_num: RevisionNum::V2(0),
        processor: Processor::BCM2837,
    };
    assert_eq!(expected, parse_num(rev_code).unwrap());
}

#[test]
fn new_4b_1g() {
    let rev_code = 0xa03111;
    let expected = Revision {
        revision_code: rev_code,
        memory: MemorySize::MiB1024,
        mfg: Manufacturer::SonyUK,
        model: Model::B4,
        revision_num: RevisionNum::V2(1),
        processor: Processor::BCM2711,
    };
    assert_eq!(expected, parse_num(rev_code).unwrap());
}

#[test]
fn new_4b_2g() {
    let rev_code = 0xb03111;
    let expected = Revision {
        revision_code: rev_code,
        memory: MemorySize::MiB2048,
        mfg: Manufacturer::SonyUK,
        model: Model::B4,
        revision_num: RevisionNum::V2(1),
        processor: Processor::BCM2711,
    };
    assert_eq!(expected, parse_num(rev_code).unwrap());
}

#[test]
fn new_4b_4g() {
    let rev_code = 0xc03111;
    let expected = Revision {
        revision_code: rev_code,
        memory: MemorySize::MiB4096,
        mfg: Manufacturer::SonyUK,
        model: Model::B4,
        revision_num: RevisionNum::V2(1),
        processor: Processor::BCM2711,
    };
    assert_eq!(expected, parse_num(rev_code).unwrap());
}

#[test]
fn cpuinfo_3_sonyuk() {
    let path = path::Path::new("src/test-data/rpi3.txt");

    let expected = RaspberryPiInfo {
        revision: Revision {
            revision_code: 0xa02082,
            memory: MemorySize::MiB1024,
            mfg: Manufacturer::SonyUK,
            model: Model::B3,
            revision_num: RevisionNum::V2(2),
            processor: Processor::BCM2837,
        },
        serial: String::from("00000000f9c7e06f"),
    };

    assert_eq!(expected, parse_cpuinfo_path(&path).unwrap().unwrap());
}

#[test]
fn cpuinfo_3_embest() {
    let path = path::Path::new("src/test-data/rpi3-embest.txt");

    let expected = RaspberryPiInfo {
        revision: Revision {
            revision_code: 0xa22082,
            memory: MemorySize::MiB1024,
            mfg: Manufacturer::Embest,
            model: Model::B3,
            revision_num: RevisionNum::V2(2),
            processor: Processor::BCM2837,
        },
        serial: String::from("000000009126ba21"),
    };

    assert_eq!(expected, parse_cpuinfo_path(&path).unwrap().unwrap());
}

#[test]
fn cpuinfo_1bplus() {
    let path = path::Path::new("src/test-data/rpi1bplus.txt");

    let expected = RaspberryPiInfo {
        revision: Revision {
            revision_code: 0x10,
            memory: MemorySize::MiB512,
            mfg: Manufacturer::SonyUK,
            model: Model::BPlus,
            revision_num: RevisionNum::V1(1, 0),
            processor: Processor::BCM2835,
        },
        serial: String::from("00000000b5787da2"),
    };

    assert_eq!(expected, parse_cpuinfo_path(&path).unwrap().unwrap());
}

#[test]
fn cpuinfo_2b() {
    let path = path::Path::new("src/test-data/rpi2b.txt");

    let expected = RaspberryPiInfo {
        revision: Revision {
            revision_code: 0xa01041,
            memory: MemorySize::MiB1024,
            mfg: Manufacturer::SonyUK,
            model: Model::B2,
            revision_num: RevisionNum::V2(1),
            processor: Processor::BCM2836,
        },
        serial: String::from("000000009668081c"),
    };

    assert_eq!(expected, parse_cpuinfo_path(&path).unwrap().unwrap());
}

#[test]
fn cpuinfo_cm3() {
    let path = path::Path::new("src/test-data/rpicm3.txt");

    let expected = RaspberryPiInfo {
        revision: Revision {
            revision_code: 0xa220a0,
            memory: MemorySize::MiB1024,
            mfg: Manufacturer::Embest,
            model: Model::CM3,
            revision_num: RevisionNum::V2(0),
            processor: Processor::BCM2837,
        },
        serial: String::from("0000000022badd1a"),
    };

    assert_eq!(expected, parse_cpuinfo_path(&path).unwrap().unwrap());
}

#[test]
fn cpuinfo_3plus() {
    let path = path::Path::new("src/test-data/rpi3-plus.txt");

    let expected = RaspberryPiInfo {
        revision: Revision {
            revision_code: 0xa020d3,
            memory: MemorySize::MiB1024,
            mfg: Manufacturer::SonyUK,
            model: Model::B3Plus,
            revision_num: RevisionNum::V2(3),
            processor: Processor::BCM2837,
        },
        serial: String::from("0000000031e54e47"),
    };

    assert_eq!(expected, parse_cpuinfo_path(&path).unwrap().unwrap());
}

#[test]
fn cpuinfo_4b() {
    let path = path::Path::new("src/test-data/rpi4b.txt");

    let expected = RaspberryPiInfo {
        revision: Revision {
            revision_code: 0xc03111,
            memory: MemorySize::MiB4096,
            mfg: Manufacturer::SonyUK,
            model: Model::B4,
            revision_num: RevisionNum::V2(1),
            processor: Processor::BCM2711,
        },
        serial: String::from("10000000c6bfcbd0"),
    };

    assert_eq!(expected, parse_cpuinfo_path(&path).unwrap().unwrap());
}

#[test]
fn model_display() {
    assert_eq!("Zero W", format!("{}", Model::ZeroW));
    assert_eq!("3B", format!("{}", Model::B3));
}

#[test]
fn mfg_display() {
    assert_eq!("Sony UK", format!("{}", Manufacturer::SonyUK));
}
