# 0.3.0
- Support pi 4B

# 0.2.0
- Expose serial number
- Reorganize public structs

# 0.1.0
- Initial release
