
[![](https://img.shields.io/crates/v/rpi-info.svg)](https://crates.io/crates/rpi-info) [![](https://docs.rs/rpi-info/badge.svg)](https://docs.rs/rpi-info/)

This crate makes it easy to determine what sort of Raspberry Pi a Linux host is. The different models have different pin layouts, etc, so it's often useful to know what model you're running on.

To see it in action, try the `show` example.
