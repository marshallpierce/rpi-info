extern crate rpi_info;

fn main() {
    match rpi_info::load_cpuinfo() {
        Ok(opt) => match opt {
            Some(i) => {
                println!(
                    "Detected a Raspberry Pi {:?} with {} MiB RAM and serial {}.",
                    i.revision.model,
                    i.revision.memory.mib(),
                    i.serial
                );
                println!("Full info:\n{:?}", i);
            }
            None => println!("You don't appear to be running this on a Raspberry Pi."),
        },
        Err(e) => {
            eprintln!("Could not read from cpuinfo: {}", e);
            std::process::exit(1);
        }
    }
}
